To install WebdriverIO run the following command:
`npm i --save-dev @wdio/cli`

To create WebdriverIO config file execute command in the terminal:

`./node_modules/.bin/wdio config -y` - all default options get set up for you. And wdio.conf.js file gets created under your project root folder. (By default, this auto-configure set services as the chromedriver, framework as Mocha with BDD options.)

`./node_modules/.bin/wdio config` - to choose displayed options according to your requirements.
a test folder in config file under `specs:` should be consistent to real - `'./test/eukanuba/**/*.js'`


To run the Eukanuba test locally execute command in the terminal:
`wdio run EukanubaLocal.conf.js`

To run the Eukanuba test on LambdaTest execute command in the terminal:
`wdio run EukanubaRemote.conf.js`
