const assert = require("assert");

describe("Eukanuba Test", () => {
    it("Eukanuba products", async () => {
        await browser.url('https://www.eukanuba.com/us')

        //wait cookie window and close it
        await (await browser.$("//div[@id = 'onetrust-banner-sdk']//button[contains(@class, 'banner-close-button onetrust-lg') or @id = 'onetrust-accept-btn-handler']"))
            .click().catch(() => {})
        //click to "Product" navigation link
        await (await browser.$("//a[contains(@href, '/products')]")).click()
        // click to "View all products" link
        await (await browser.$("//a[contains(text(),'View all products')]")).click()
        // click to "Senior Small Breed Dry Dog Food" product
        await (await browser.$("//a[contains(@data-product-details, 'Senior Small Breed Dry Dog Food')]//span[contains(text(), 'View Product')]")).click()

        await (await browser.$("//div[contains(@data-track-custom-category,'Buy Now')]"))
            .waitForExist({timeoutMsg : "Attention! Current product cannot be bought!"})

    });
});